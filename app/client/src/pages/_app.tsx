import type { AppProps } from "next/app";
import Head from "next/head";
import CssBaseline from "@material-ui/core/CssBaseline";
import { SnackbarProvider } from "notistack";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { connected } from "src/helpers";

const App = ({ Component, pageProps }: AppProps) => {
	const router = useRouter();

	useEffect(() => {
		if (connected()) {
			router.push("dashboard", "/dashboard");
		}
	}, []);

	return (
		<>
			<Head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width,initial-scale=1" />
				<meta httpEquiv="X-UA-Compatible" content="ie=edge" />
				<meta name="robots" content="noindex,nofollow" />

				<title>MONOREPOC</title>
			</Head>

			<CssBaseline />

			<SnackbarProvider maxSnack={2}>
				<Component {...pageProps} />
			</SnackbarProvider>
		</>
	);
};

export default App;
