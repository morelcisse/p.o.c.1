import { gql } from "@apollo/client";
import { apolloClient } from "common/helpers/Apollo";
import Button from "@material-ui/core/Button";
import Link from "next/link";
import { commonStyles } from "src/styles/main";
import { TextField } from "formik-material-ui";
import { Formik, Form, Field } from "formik";
import CenteredLayout from "src/components/Layout/GridLayout";
import { Box } from "@material-ui/core";
import { useRouter } from "next/router";
import { IUser } from "common/interfaces/User";
import { useState } from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { useSnackbar } from "notistack";

const Login = () => {
	const [submited, setSubmited] = useState(false);
	const { enqueueSnackbar } = useSnackbar();
	const classes = commonStyles();
	const router = useRouter();
	const onSubmit = (values: IUser) => {
		setSubmited(true);
		apolloClient
			.query({
				query: gql`
					query ($email: String!, $password: String!) {
						login(user: { email: $email, password: $password }) {
							... on MsgType {
								message
								error
							}
							... on UserType {
								email
								firstname
								lastname
								access_token
								connected
							}
						}
					}
				`,
				variables: { ...values },
			})
			.then(({ data }) => {
				const result = data.login;

				if (result.__typename === "MsgType" && result.error) {
					const error = result;
					const message = error.message;
					enqueueSnackbar(message, { variant: "error" });
					setSubmited(false);
				} else {
					const user: IUser = result;

					window.localStorage.setItem("token", user.access_token);
					window.localStorage.setItem("username", `${user.firstname} ${user.lastname}`);
					enqueueSnackbar("Vous êtes connecté", {
						variant: "success",
						autoHideDuration: 1000,
					});
					setTimeout(() => {
						router.push("dashboard", "/dashboard");
					}, 500);
				}
			});
	};
	const validate = (values: IUser) => {
		const errors: Partial<IUser> = {};

		if (!values.email) {
			errors.email = "Ce champs es requis";
		}
		if (!values.password) {
			errors.password = "Ce champs es requis";
		}

		return errors;
	};

	return (
		<CenteredLayout title="CONNECTEZ-VOUS">
			{submited && <LinearProgress color="secondary" />}
			<Formik
				initialValues={{ email: "", password: "" } as IUser}
				validate={validate}
				onSubmit={onSubmit}
			>
				{({ touched, errors }) => (
					<Form className={classes.form}>
						<Field
							component={TextField}
							name="email"
							type="email"
							id="login-email"
							label="Adresse email"
							fullWidth
							error={touched["email"] && !!errors["email"]}
							helperText={touched["email"] && errors["email"]}
							variant="outlined"
							margin="normal"
							disabled={submited}
						/>
						<br />
						<Field
							component={TextField}
							id="login-password"
							type="password"
							label="Mot de passe"
							name="password"
							fullWidth
							error={touched["password"] && !!errors["password"]}
							helperText={touched["password"] && errors["password"]}
							variant="outlined"
							margin="normal"
							disabled={submited}
						/>
						<br />
						<Box className={classes.submitBox}>
							<Button
								type="submit"
								id="login-submit"
								variant="contained"
								color="secondary"
								className={classes.submit}
								disabled={submited}
							>
								Connexion
							</Button>
						</Box>
						<br />
						<small className={classes.link}>
							Vous n'avez pas de compte ?{" "}
							<Link href="register" as="/register">
								Inscrivez-vous
							</Link>
						</small>
					</Form>
				)}
			</Formik>
		</CenteredLayout>
	);
};

export default Login;
