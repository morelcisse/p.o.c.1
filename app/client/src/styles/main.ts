import { createStyles, makeStyles, Theme } from "@material-ui/core";

const commonStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			flexGrow: 1,
		},
		menuButton: {
			marginRight: theme.spacing(2),
		},
		title: {
			textAlign: "center",
			textTransform: "uppercase",
			fontWeight: "bold",
			textDecoration: "underline",
			marginBottom: 20,
		},
		paper: {
			padding: theme.spacing(2),
			textAlign: "center",
			color: theme.palette.text.secondary,
		},
		navTitle: {
			flexGrow: 1,
		},
		dashContainer: {
			minHeight: "100vh",
			backgroundColor: "#eceff1",
			padding: 0,
		},
		button: {
			margin: theme.spacing(2),
		},
		form: {
			width: "100%",
			marginTop: theme.spacing(1),
		},
		submit: {
			margin: theme.spacing(3, 0, 2),
		},
		submitBox: {
			display: "flex",
			justifyContent: "center",
			flexDirection: "row",
		},
		link: {
			display: "block",
			textAlign: "center",
			textDecoration: "none",
		},
		linkNone: {
			textDecoration: "none",
		},
	})
);

export { commonStyles };
