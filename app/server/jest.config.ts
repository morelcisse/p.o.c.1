module.exports = {
	preset: "ts-jest",
	transform: {
		"<transform_regex>": [
			"ts-jest",
			{
				tsconfig: "tsconfig.json",
				isolatedModules: false,
			},
		],
	},
	testEnvironment: "node",
	rootDir: ".",
	setupFilesAfterEnv: ["<rootDir>/setup-files.js"],
	collectCoverageFrom: ["./src/**/*.ts", "!**/graphql/**", "!**/node_modules/**"],
};
